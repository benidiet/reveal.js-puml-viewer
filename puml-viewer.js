
const Plugin = () => {

    let deck;

    
    function insertAfter(newNode, referenceNode) {
        referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
    }

    function openPumlViewer() {

        var elements = document.querySelectorAll('[data-puml-code]');

        elements.forEach(function(element) {
            var el = document.createElement("pre");
            el.style.display="none";
            var pumlCode = document.createElement("code");
            var str = element.getAttribute("data-puml-code");
            str = str.replace('"@startuml', '@startuml');
            str = str.replace('@enduml"', '@enduml');
            
            str = str.replace(/(?:\\r\\n|\\r|\\n)/g, '\n');     // Replace \\n with \n
            str = str.replace(/(?:\\")/g, '"');                 // Replace \" with "
            str = str.replace(/(?:\\")/g, '"');                 // Replace \" with "
            pumlCode.innerText =str;
            el.appendChild(pumlCode);
            insertAfter(el, element);

            el.ondblclick = (function(){
                if (el.style.display =="none") {
                    element.style.display = "none";
                    el.style.display = "";
                }
                else {
                    element.style.display = "";
                    el.style.display = "none";
                }
            });

            element.ondblclick = (function(){
                if (el.style.display =="none") {
                    element.style.display = "none";
                    el.style.display = "";
                }
                else {
                    element.style.display = "";
                    el.style.display = "none";
                }
            });

        });
    }

    return {
        id: 'PumlViewer',

        init: function (reveal) {

            deck = reveal;

            openPumlViewer();
        },

        open: openPumlViewer
    };
};

export default Plugin;
